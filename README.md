# 险途开源模块车模

#### 介绍
险途开源模块车模,让车模回归大众！

#### 架构
架构说明
整体分为车身结构，车身控制、图传、遥控、反馈五部分

#### 系统教程

1.  车身结构
        车身结构采用3D打印技术结合铝型材加强
2.  车身控制
        车身控制采用惯性传感器+gps对车辆进行姿态控制
3.  图传
        图传系统直接搬运树莓派的开源项目
4.  遥控
        遥控部分两条腿走路，一套现有开源系统，一套自研
5.  反馈
        当车身发生碰撞等向遥控器返回数据

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
